import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
//services importd
import AppointmentsList from "./Services/AppointmentsList";
import AppointmentForm from "./Services/AppointmentForm";
import TechniciansList from "./Services/TechniciansList";
import TechnicianForm from "./Services/TechnicianForm";
import ServiceHistoryList from "./Services/ServiceHistoryList";
// inventory imports
import ManufacturerForm from "./Inventory/ManufacturerForm";
import ManufacturersList from "./Inventory/ManufacturersList";
import ModelsList from "./Inventory/ModelsList";
import ModelForm from "./Inventory/ModelForm";
import ListAutomobiles from "./Inventory/ListAutomobiles";
import AutomobileForm from "./Inventory/AutomobileForm";
// sales imports
import SalesPeopleList from "./Sales/ListSalesperson";
import EmployeeForm from "./Sales/SalespersonForm";
import CustomerForm from "./Sales/CustomerForm";
import CustomerList from "./Sales/ListCustomer";
import SalesHistory from "./Sales/SalespersonHistory"
import SaleForm from "./Sales/SaleForm"
import SalesList from "./Sales/ListSales"

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/*****************Services Routes***************/}
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          {/*****************Inventory Routes***************/}
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
          {/*****************Sales Routes***************/}
          <Route path="/salespeople" element={<SalesPeopleList />} />
          <Route path="/salespeople/create" element={<EmployeeForm />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/sales/create" element={< SaleForm/>} />
          <Route path="/sales" element={< SalesList/>} />
          <Route path="/sales/history" element={< SalesHistory/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
