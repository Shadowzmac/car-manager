import { useState, useEffect } from "react";

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (event, id) => {
    event.preventDefault();
    const manufacturer_url = `http://localhost:8100/api/manufacturers/${id}/`;

    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturer_url, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div>
      <h1 style={{ paddingTop: 20 }}>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                <td>
                  <button
                    onClick={(event) => handleDelete(event, manufacturer.id)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ManufacturersList;
