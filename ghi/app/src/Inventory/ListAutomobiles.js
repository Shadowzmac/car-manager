import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function ListAutomobiles(){
    const [autos, setAutomobiles] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok){
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const deleteAutomobiles = async (id) => {
        const delAutomobileUrl = `http://localhost:8100/api/automobiles/${id}/`;
        const fetchConfig = {
            method:"DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(delAutomobileUrl, fetchConfig);
        if (response.ok) {
            getData()
        }
      }

    return (
        <div>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{ auto.vin }</td>
                                <td>{ auto.color }</td>
                                <td>{ auto.year }</td>
                                <td>{ auto.model.name }</td>
                                <td>{ auto.model.manufacturer.name }</td>
                                <td>{ auto.sold ? "Yes" : "No"}</td>
                                <td><button className= "btn btn-danger" onClick={() => deleteAutomobiles(auto.id)}>Remove</button> </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <Link to="/automobiles/create">
                <button className="btn btn-primary">Add New Automobile</button>
            </Link>
        </div>
      )
}

export default ListAutomobiles