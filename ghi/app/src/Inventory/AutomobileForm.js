import React, {useState, useEffect } from 'react';

function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model: '',
      })

      const [submissionStatus, setSubmissionStatus] = useState(null);
      const fetchData = async () => {
        const modelurl = "http://localhost:8100/api/models/";
        const response = await fetch(modelurl);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      };
      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        const automobileUrl = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        try {
          const response = await fetch(automobileUrl, fetchConfig);

          if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model: '',
            });
            setSubmissionStatus('success');
          } else {
            setSubmissionStatus('error');
          }
            } catch (error) {
          console.error('Error:', error);
          setSubmissionStatus('error'); // Set error status in case of network error
        }
      }

      const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      }



      return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className="shadow p-4 mt-4">
                    <h1>Add a Automobile</h1>
                    {submissionStatus === 'success' && (
                      <div className="alert alert-success">Automobile created successfully!</div>
                    )}
                    {submissionStatus === 'error' && (
                      <div className="alert alert-danger">Failed to create Automobile. Please try again.</div>
                    )}

                    <form onSubmit={handleSubmit} id='create-customer-form'>
                        <div className='form-floating mb-3'>
                            <input
                              onChange={handleFormChange}
                              placeholder="Color"
                              required type="text"
                              name="color"
                              id="color"
                              className="form-control"
                              value={formData.color}
                            />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input
                             value={formData.year}
                             onChange={handleFormChange}
                             placeholder="Year"
                             required type="text"
                             name="year"
                             id="year"
                             className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input
                             value={formData.address}
                             onChange={handleFormChange}
                             placeholder="Vin"
                             required type="text"
                             name="vin"
                             id="vin"
                             className="form-control" />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleFormChange}
                                required
                                name="model_id"
                                id="model_id"
                                className="form-select"
                                value={formData.model_id}
                            >
                                <option value="">Choose a model...</option>
                                {models.map((model) => {
                                return (
                                    <option key={model.id} value={model.id}>
                                    {model.name}
                                    </option>
                                );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
      )
}
export default AutomobileForm;