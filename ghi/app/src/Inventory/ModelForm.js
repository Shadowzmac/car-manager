import React, { useState, useEffect } from "react";

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([]);

  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });

  const fetchData = async () => {
    const manufacturer_url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturer_url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const models_url = "http://localhost:8100/api/models/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(models_url, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        picture_url: "",
        manufacturer_id: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Vehicle Model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input
                placeholder="Model Name"
                onChange={handleFormChange}
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={formData.name}
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Picture URL"
                onChange={handleFormChange}
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
                value={formData.picture_url}
              />
              <label htmlFor="name">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                required
                name="manufacturer_id"
                id="manufacturer_id"
                className="form-select"
                value={formData.manufacturer_id}
              >
                <option value="">Choose a manufacturer...</option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ModelForm;
