import { useState, useEffect } from "react";

function ModelsList() {
  const [models, setModels] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (event, id) => {
    event.preventDefault();
    const model_url = `http://localhost:8100/api/models/${id}/`;

    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(model_url, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div>
      <h1 style={{ paddingTop: 20 }}>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    style={{ width: "auto", height: "100px" }}
                    alt="Model Image"
                  />
                </td>
                <td>
                  <button
                    onClick={(event) => handleDelete(event, model.id)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ModelsList;
