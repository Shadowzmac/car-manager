import { useState, useEffect } from "react";

function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([]);
  const [list, setList] = useState([]);
  const [query, setQuery] = useState('');

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      setList(data.appointments);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  
  const handleCreated = async (event, id) => {
    event.preventDefault();
    const appointment_url = `http://localhost:8080/api/appointments/${id}/created/`;

    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointment_url, fetchConfig);
    if (response.ok) {
      getData();
    }
  };
  const handleChange =  (event) => {
      const results = appointments.filter(appointment => {
        if (event.target.value === "") return appointments
        return appointment.vin.toLowerCase().includes(event.target.value.toLowerCase())
      })
      setQuery(event.target.value)
      setList(results)
  }
  return (
    <div>
      <h1 style={{ paddingTop: "2%" }}>Service History</h1>
      <div style={{ paddingTop: "2%" }}>
        <input className="form-control" onChange={handleChange} value={query} type="search" placeholder="Search by VIN . . ." />
      </div>
      <div style={{ paddingTop: "2%" }}>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>VIP</th>
              <th>Customer</th>
              <th>Date/Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Current Status</th>
              <th>Revert Status</th>
            </tr>
          </thead>
          <tbody id="myTable">
            {list.map((appointment) => {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.vip ? "⭐" : " "}</td>
                  <td>{appointment.customer}</td>
                  <td>{new Date(appointment.date_time).toLocaleString('en-US', { timeZone: 'UTC' })}</td>
                  <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                  <td>{appointment.reason}</td>
                  <td>{appointment.status}</td>
                  <td style={{ paddingLeft: "2%" }}>
                  <button onClick={(event) => handleCreated(event, appointment.id)} className="btn btn-success" style={{ width: "auto", height: "40px" }}>Created</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default ServiceHistoryList;
