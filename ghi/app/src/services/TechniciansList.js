import {useState, useEffect} from "react";

function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };
    useEffect(() => {
        getData();
    }, []);

    const handleDelete = async (event, id) => {
        event.preventDefault();
        const technician_url = `http://localhost:8080/api/technicians/${id}/`;

        const fetchConfig = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(technician_url, fetchConfig);
        if (response.ok) {
            getData();
        }
    };

    return (
        <div>
            <h1 style={{ paddingTop: 20 }}>Technicians</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map((technician) => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td><button onClick={(event) => handleDelete(event, technician.id)} className="btn btn-danger" style={{ width: "auto", height: "40px" }}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default TechniciansList;
