import React, { useState, useEffect } from "react";

function AppointmentForm() {
  const [technicians, setTechnicians] = useState([]);

  const [formData, setFormData] = useState({
    date_time: "",
    reason: "",
    vin: "",
    customer: "",
    technician_id: "",
  });

  const fetchData = async () => {
    const technician_url = "http://localhost:8080/api/technicians/";
    const response = await fetch(technician_url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const appointments_url = "http://localhost:8080/api/appointments/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointments_url, fetchConfig);
    if (response.ok) {
      setFormData({
        date_time: "",
        reason: "",
        vin: "",
        customer: "",
        technician_id: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 style={{ paddingBottom: 20 }}>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                placeholder="VIN"
                onChange={handleFormChange}
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
                value={formData.vin}
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Customer"
                onChange={handleFormChange}
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
                value={formData.customer}
              />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Date/Time"
                onChange={handleFormChange}
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
                value={formData.date_time}
              />
              <label htmlFor="date_time">Date/Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                required
                name="technician_id"
                id="technician_id"
                className="form-select"
                value={formData.technician_id}
              >
                <option value="">Choose a technician...</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.first_name} {technician.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Reason"
                onChange={handleFormChange}
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
                value={formData.reason}
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AppointmentForm;
