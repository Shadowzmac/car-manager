import React, { useState } from "react";

function TechnicianForm() {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });
  const handleSubmit = async (event) => {
    event.preventDefault();

    const technicians_url = "http://localhost:8080/api/technicians/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(technicians_url, fetchConfig);
    if (response.ok) {
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                placeholder="First Name"
                onChange={handleFormChange}
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                value={formData.first_name}
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Last Name"
                onChange={handleFormChange}
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                value={formData.last_name}
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Employee ID"
                onChange={handleFormChange}
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                value={formData.employee_id}
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default TechnicianForm;
