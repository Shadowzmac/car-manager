import { useState, useEffect } from "react";

function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleCancel = async (event, id) => {
    event.preventDefault();
    const appointment_url = `http://localhost:8080/api/appointments/${id}/cancel/`;

    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointment_url, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  const handleFinish = async (event, id) => {
    event.preventDefault();
    const appointment_url = `http://localhost:8080/api/appointments/${id}/finish/`;

    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointment_url, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div>
      <h1 style={{ paddingTop: 20 }}>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Update Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.filter((appointment) => appointment.status === "created").map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip ? "⭐" : " "}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleString('en-US', { timeZone: 'UTC' })}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button onClick={(event) => handleCancel(event, appointment.id)} className="btn btn-danger" style={{ width: "auto", height: "40px", marginRight: "1%" }}>Cancel</button>
                  <button onClick={(event) => handleFinish(event, appointment.id)} className="btn btn-success" style={{ width: "auto", height: "40px", marginLeft: "1%" }}>Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default AppointmentsList;
