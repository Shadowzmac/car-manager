import React, {useState, useEffect } from 'react';

function EmployeeForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
      })

      const [submissionStatus, setSubmissionStatus] = useState(null);

      const handleSubmit = async (event) => {
        event.preventDefault();

        const employeeUrl = 'http://localhost:8090/api/salespeople/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        try {
          const response = await fetch(employeeUrl, fetchConfig);

          if (response.ok) {
            setFormData({
              first_name: '',
              last_name: '',
              employee_id: '',
            });
            setSubmissionStatus('success');
          } else {
            setSubmissionStatus('error');
          }
            } catch (error) {
          console.error('Error:', error);
          setSubmissionStatus('error'); // Set error status in case of network error
        }
      }

      const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      }



      return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    {submissionStatus === 'success' && (
                      <div className="alert alert-success">Salesperson created successfully!</div>
                    )}
                    {submissionStatus === 'error' && (
                      <div className="alert alert-danger">Failed to create salesperson. Please try again.</div>
                    )}

                    <form onSubmit={handleSubmit} id='create-employee-form'>
                        <div className='form-floating mb-3'>
                            <input
                              onChange={handleFormChange}
                              placeholder="First Name"
                              required type="text"
                              name="first_name"
                              id="first_name"
                              className="form-control"
                              value={formData.first_name}
                            />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={formData.last_name} onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={formData.employee_id} onChange={handleFormChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
      )
}
export default EmployeeForm;