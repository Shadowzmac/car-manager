import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function SalesList(){
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales');

        if (response.ok){
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const deleteSales = async (id) => {
        const delSalesUrl = `http://localhost:8090/api/sales/${id}/`;
        const fetchConfig = {
            method:"DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(delSalesUrl, fetchConfig);
        if (response.ok) {
            getData()
        }
      }

    return (
        <div>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Saleperson Name</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.salesperson.first_name }</td>
                                <td>{ sale.customer.first_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ sale.price }</td>
                                <td><button className= "btn btn-danger" onClick={() => deleteSales(sale.id)}>Remove</button> </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <Link to="/sales/create">
                <button className="btn btn-primary">Add New Sale</button>
            </Link>
        </div>
      )
}

export default SalesList