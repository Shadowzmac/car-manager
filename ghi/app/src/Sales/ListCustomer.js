import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function CustomerList(){
    const [customer, setCustomer] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers');

        if (response.ok){
            const data = await response.json();
            setCustomer(data.customer)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const deleteCustomer = async (id) => {
        const delCustomerUrl = `http://localhost:8090/api/customers/${id}/`;
        const fetchConfig = {
            method:"DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(delCustomerUrl, fetchConfig);
        if (response.ok) {
            getData()
        }
      }

    return (
        <div>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map(employee => {
                        return (
                            <tr key={employee.id}>
                                <td>{ employee.first_name }</td>
                                <td>{ employee.last_name }</td>
                                <td>{ employee.phone_number }</td>
                                <td>{ employee.address }</td>
                                <td><button className= "btn btn-danger" onClick={() => deleteCustomer(employee.id)}>Remove</button> </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <Link to="/customers/create">
                <button className="btn btn-primary">Add New Customer</button>
            </Link>
        </div>
      )
}

export default CustomerList