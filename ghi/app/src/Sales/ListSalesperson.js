import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function SalesPeopleList(){
    const [salesperson, setSalesperson] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople');

        if (response.ok){
            const data = await response.json();
            setSalesperson(data.salesperson)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const deleteSalesperson = async (id) => {
        const delSalespersonUrl = `http://localhost:8090/api/salespeople/${id}/`;
        const fetchConfig = {
            method:"DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(delSalespersonUrl, fetchConfig);
        if (response.ok) {
            getData()
        }
      }

    return (
        <div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson.map(employee => {
                        return (
                            <tr key={employee.id}>
                                <td>{ employee.employee_id }</td>
                                <td>{ employee.first_name }</td>
                                <td>{ employee.last_name }</td>
                                <td><button className= "btn btn-danger" onClick={() => deleteSalesperson(employee.id)}>Remove</button> </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <Link to="/salespeople/create">
                <button className="btn btn-primary">Add New Employee</button>
            </Link>
        </div>
      )
}

export default SalesPeopleList