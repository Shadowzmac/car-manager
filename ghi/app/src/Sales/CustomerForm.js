import React, {useState, useEffect } from 'react';

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
      })

      const [submissionStatus, setSubmissionStatus] = useState(null);

      const handleSubmit = async (event) => {
        event.preventDefault();

        const customerUrl = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        try {
          const response = await fetch(customerUrl, fetchConfig);

          if (response.ok) {
            setFormData({
              first_name: '',
              last_name: '',
              address: '',
              phone_number: '',
            });
            setSubmissionStatus('success');
          } else {
            setSubmissionStatus('error');
          }
            } catch (error) {
          console.error('Error:', error);
          setSubmissionStatus('error'); // Set error status in case of network error
        }
      }

      const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      }



      return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    {submissionStatus === 'success' && (
                      <div className="alert alert-success">Customer created successfully!</div>
                    )}
                    {submissionStatus === 'error' && (
                      <div className="alert alert-danger">Failed to create Customer. Please try again.</div>
                    )}

                    <form onSubmit={handleSubmit} id='create-customer-form'>
                        <div className='form-floating mb-3'>
                            <input
                              onChange={handleFormChange}
                              placeholder="First Name"
                              required type="text"
                              name="first_name"
                              id="first_name"
                              className="form-control"
                              value={formData.first_name}
                            />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input
                             value={formData.last_name}
                             onChange={handleFormChange}
                             placeholder="Last Name"
                             required type="text"
                             name="last_name"
                             id="last_name"
                             className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input
                             value={formData.address}
                             onChange={handleFormChange}
                             placeholder="Address"
                             required type="text"
                             name="address"
                             id="address"
                             className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input
                             value={formData.phone_number}
                             onChange={handleFormChange}
                             placeholder="Phone Number"
                             required type="text"
                             name="phone_number"
                             id="phone_number"
                             className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
      )
}
export default CustomerForm;