import { useEffect, useState } from 'react';

function SalesHistory(){
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState('')

    const handleEmployeeChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const getSales = async () => {
        const salesurl = ('http://localhost:8090/api/sales');
        const response = await fetch(salesurl)
        if (response.ok){
            const data = await response.json();
            setSales(data.sales)
        }
    }

    const getSalespeople = async () => {
        const salespeopleurl = ('http://localhost:8090/api/salespeople');
        const response = await fetch(salespeopleurl)
        if (response.ok){
            const data = await response.json();
            setSalespeople(data.salesperson)

        }
    }

    useEffect(()=>{
        getSales();
        getSalespeople();
    }, [])


    return (
        <div>
            <br></br>
            <h1>Salesperson History</h1>
            <br></br>
            <select value={salesperson} onChange={handleEmployeeChange} required name="salespeople">
                <option value="" >Choose A Salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {`${salesperson.first_name} ${salesperson.last_name}`}
                        </option>
                    )
                })}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter((sale) => sale.salesperson.employee_id === salesperson).map(sale => {
                        return (
                            <tr key={sale.id} value = {sale.id}>
                                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ `$${sale.price}.00` }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
      )
}

export default SalesHistory