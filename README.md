# CarCar

CarCar is an application auto dealers can use to manage thier dealership. This application will manage inventory, sales, and services.

Team:

- Zac McClarnon - Sales
- Nadine Castillo - Services

## Getting Started

1. Fork this repository: https://gitlab.com/NadineC/project-beta

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/NadineC/project-beta.git

3. Build and run the project using Docker with these commands:

docker volume create beta-data
docker-compose build
docker-compose up

4. Make sure all of your Docker containers are running

5. View the project in the browser: http://localhost:3000/


## Design

3 microservices (Inventory, Services, Sales) all interact with each other

![Diagram](CarCarDiagram.png)

## Inventory microservice

**NOTE: You must create a manufacturer, then you can create a vehicle model ONLY when both a manufacturer and model have been created are you able to create an automobile**

The inventory microservice allows you to create/delete manufacturers and view a list of manufacturers. It also allows you to create/delete vehicle models and view the models in a list as well. After you have a manufacturer and model created, you can now create an automobile. This allows the dealership to easily keep track of which manufacturers/models and automobiles are currently or in the past were at the dealership.

**URLs**
urlpatterns = [
    path("automobiles/", api_automobiles, name="api_automobiles",),
    path("automobiles/<str:vin>/", api_automobile, name="api_automobile",),
    path("manufacturers/", api_manufacturers, name="api_manufacturers",),
    path("manufacturers/<int:pk>/", api_manufacturer, name="api_manufacturer",),
    path("models/", api_vehicle_models, name="api_vehicle_models",),
    path("models/<int:pk>/", api_vehicle_model, name="api_vehicle_model",),
]

**Ports**
react-1 3000:3000 | service-api-1 8080:8000 | sales-api-1 8090:8000 | inventory-api-1 8100:8000 | database-1 15432:5432

**Inventory API**

Manufacturer**

Create Manufacturer
POST || URL || http://localhost:8100/api/manufacturers/
Insomnia input:

{
  "name": "Chrysler"
}

List Manufacturer
GET || URL || http://localhost:8100/api/manufacturers/
Insomnia input:

DELETE Manufacturer
DELETE || URL || http://localhost:8100/api/manufacturers/1/
Insomnia input:

VehicleModel**

Create VehicleModel
POST || URL || http://localhost:8100/api/models/
Insomnia input:

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

List VehicleModel
GET || URL || http://localhost:8100/api/models/
Insomnia input:

DELETE VehicleModel
DELETE || URL || http://localhost:8100/api/models/1/
Insomnia input:

Automobile**

Create Automobile
POST || URL || http://localhost:8100/api/automobiles/
Insomnia input:

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

List Automobile
GET || URL || http://localhost:8100/api/automobiles/
Insomnia input:

DELETE Automobile
DELETE || URL || http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/
Insomnia input:

**Models**

class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)

class VehicleModel(models.Model):
    name = models.CharField(max_length=100)
    picture_url = models.URLField()

    manufacturer = models.ForeignKey(
        Manufacturer,
        related_name="models",
        on_delete=models.CASCADE,
    )

class Automobile(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    model = models.ForeignKey(
        VehicleModel,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )

## Service microservice

The service microservice manages the appointments / technicians data. You can create/delete technicians and also view a list of technicians. Appointments can be created and can be viewed by a list of current appointments. Or by a list of all appointments including newly created, finished, or canceled appointments. If an appointment's status is changed to cancel or finished the appointment will no longer show on the list of current appointments, however say you accidently updated that status to canceled or finished, no worries you can go to the history list and revert the status back to created and it will show on the current appointment list again. Also by using our poller we are able to see if the vehicle scheduled for the auto service was sold at our dealership, the both lists will show a star in the  VIP section of customers who purchased thier vehicle from our dealership. We gather this information by checking if the vin listed on the appointment matches a vin that was in our inventory microservice, this is checked as soon as the appointment is made. Also if the customer has had maintenance with us previously we can search our history list of appointments to give us additional insight of the condition of the automobile and how we can keep it in tip top shape.

**URLs**
urlpatterns = [
    path("automobiles/", api_show_automobiles, name="api_show_automobiles"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("appointments/<int:pk>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<int:pk>/finish/", api_finish_appointment, name="api_finish_appointment"),
    path("appointments/<int:pk>/created/", api_created_appointment, name="api_created_appointment"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_technician, name="api_show_technician"),
]

**Ports**
react-1 3000:3000 | service-api-1 8080:8000 | sales-api-1 8090:8000 | inventory-api-1 8100:8000 | database-1 15432:5432

**Service API**

Technician**

Create Technician
POST || URL || http://localhost:8080/api/technicians/
Insomnia input:

{
	"first_name": "Sponge",
	"last_name": "Bob",
	"employee_id": 1
}

List Technician
GET || URL || http://localhost:8080/api/technicians/
Insomnia input:

DELETE Technician
DELETE || URL || http://localhost:8080/api/technicians/1/
Insomnia input:

Appointment**

Create Appointment
POST || URL || http://localhost:8080/api/appointments/
Insomnia input:

{
	"date_time": "2024-01-23 01:30",
	"reason": "Gas",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Cat",
	"technician_id": 1
}

List Appointment
GET || URL || http://localhost:8080/api/appointments/
Insomnia input:

DELETE Appointment
DELETE || URL || http://localhost:8080/api/appointments/1/
Insomnia input:

Appointment: Update Status**

Create Appointment
PUT || URL || http://localhost:8080/api/appointments/1/created/
Insomnia input:

Cancel Appointment
PUT || URL || http://localhost:8080/api/appointments/1/cancel/
Insomnia input:

Finish Appointment
PUT || URL || http://localhost:8080/api/appointments/1/finish/
Insomnia input:

**Models**

class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=100)


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="created")
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )


## Sales microservice
Sales Routes

|http method| url                                      |function          |
|-----------|------------------------------------------|------------------|
|GET        | http://localhost:3000/                   |Main Page         |
|GET        | http://localhost:3000/customers          |List Customers    |
|POST       | http://localhost:3000/customers/create   |Create Customer   |
|GET        | http://localhost:3000/salespeople        |List Salespeople  |
|POST       | http://localhost:3000/salespeople/create |Create Salesperson|
|GET        | http://localhost:3000/sales              |List Sales        |
|POST       | http://localhost:3000/sales/create       |Create Sale       |
|GET        | http://localhost:3000/sales/history      |Sales History     |


so i plan to go through a step by step guide to get set up to run my microservice outside of react then i will explain the react side of it.
so getting set up i have the models for sales, customer, salesperson and a VO for automobile so i can add shared functionallity with the inventory back end, so i will be going step by step through how to get set up through insomnia even though with the react front end you wont need to but incase youd like to.
for insomnia all methods used the links that were provided with the project instructions i will just provide the json bodies for the POST methods that i used

Insomnia input information
<details>
Salesperson
        POST
        for post i added this information
        {
        "first_name": "matt",
        "last_name": "baltzer",
        "employee_id": "mbaltzer"
        }
        which after creation will assign an id to that salesperson
    Customer
        POST
        using this json body
        {
        "first_name": "matt",
        "last_name": "baltzer",
        "address": "billy bob lane",
        "phone_number": "5555555555"
        }
    Automobile
        POST
        this one required a little more prep work given that i needed to create a model and inside the model i created a manufacturer
        Manufacturer
        {
        "name": "Toyota"
        }
        Model
        {
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer_id": 1
        }
        Automobile
        {
        "color": "blue",
        "year": 2012,
        "vin": "1C3CC5FB2AN128888",
        "model_id": 2
        }
</details>

aside from the insomnia set up there shouldnt be much prep work that needs to be done from the github repo
DISCLAIMER!! i am unsure what caused it but a chrome browser extension caused my webpage to throw a CORS error for some reason dispite the fact it worked perfectly fine on edge or any other browser and it only happened once i solved it by turning all extensions off and back on in case you run into that error

Steps for testing the sales microservice
<details>
    starting from the first page on localhost:3000 you will be greeted with CarCar and a navbar at the top of the page
    1: if you have not added any information through insomnia the other pages will have nothing to list so first i recommend going to the New Salesperson link to add your first salesperson, following the examples given i have been seting Employee ID as first inital of first name and then your last name but if you would perfer to have your employee id be numbers or a combination those wil also work
    2: after successfully creating your first sales person you can click the Salespeople link on the top of the page to see the list of all created salespeople, on this page you have a button to remove salespoeple who no longe work for you or if there was a mistake, you can also click the button at the bottom to create a new Employee
    3: following the same pattern you will also have to add a new customer at the top of the page you will find a new customer nav which will allow you to create a new customer
    4: after creating a customer you can click on the customers nav link which will take you to a list of customers i felt this page especially was more of a buisness end section given the personal information so i also added a remove button for any reason needed to delete a customer (error, repeat, etc) i also allowed for customers to not imput an address if they would like not to or there was a housing issue so that section is allowed to be blank where as the other three are not
    5: before you can move on to creating a new sale you need vehicles in your inventory so you can sell them to your customers, first you will go to the create a manufacturer navlink and add you Makes (toyota, chevorlet, nissan, subaru, etc) you can see the list of all the created ones in the manufacturers nav link
    6: then you will need to create a vehicle model in the create a model nav link you can enter a model name such as envoy, highlander or whatever specific make and model you desire you will have to find a photo on the internet to input the url for it and finally you can select you manufacturer from the ones you created earlier
    7: finally in set up before sales you can create an automobile following the create automobile nav link which you can then imput your inventory specific information for the vehicle you would like to add, after creation you can view your vehicle in the automobile list navlink and after you see it you are able to tell whats been sold or not the default being not sold for new cars and then you can move on to your first sale
    8: following the new sale navlink you can choose a vin from your inventory, a salesperson from your employee list, a customer from your customer list and finally you can assign the agreed to selling price for the vehicle you have selected, note only the non sold vins will populate and after you hit create it will automatically change the sold to yes back on the automobile list page
    9: on the sales list navlink you can see all of the sales for the company as a whole and in the sales history navlink you can select the salesperson of your choice to see information on what sales if any that specific salesperson has made
</details>