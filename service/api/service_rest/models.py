from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return self.lfirst_name

    class Meta:
        ordering = ("last_name",)


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("vin",)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="created")
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def created(self):
        appointment = Appointment.objects.get(id=self.id)
        appointment.status = "created"
        appointment.save()

    def finish(self):
        appointment = Appointment.objects.get(id=self.id)
        appointment.status = "finished"
        appointment.save()

    def cancel(self):
        appointment = Appointment.objects.get(id=self.id)
        appointment.status = "canceled"
        appointment.save()

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("date_time",)
