from django.contrib import admin
from .models import Technician, Appointment


# Register your models here.


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    "first_name",
    "last_name",
    "employee_id",


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    "date",
    "time",
    "reason",
    "status",
    "vin",
    "customer",
